<?php

namespace FtpClient\Connector;

use Throwable;

class LoginFailedException extends \RuntimeException
{
    public function __construct(string $message = '', int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
