<?php

namespace FtpClient;

/**
 * The basic command set for any FTP operations using the Client.
 */
interface CommandInterface
{
    public function get(string $remote, string $local): bool;

    public function put(string $local, string $remote): bool;

    public function rename(string $from, string $to): bool;

    public function delete(string $path, bool $recursive = false): bool|array;

    public function list(string $directory, bool $recursive = false, bool $showHidden = false): array|false;

    public function isFile(string $filename): bool;

    public function isDir(string $directory): bool;

    public function mkdir(string $directory, bool $recursive = false, int $permissions = 0775): bool;

    public function chmod(string $path, int $permissions): bool;
}
