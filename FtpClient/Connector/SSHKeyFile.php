<?php

namespace FtpClient\Connector;

/**
 * A helper class to provide all necessary arguments for a SSH login with key-file.
 *
 * Currently PHP only supports keys in RSA and DSS format.
 *
 * The provided `$privateKeyFile` must be a path to the private key file for the certificate login.
 * A tilde `~` is expanded with the $HOME environment variable.
 * The public key file is guessed by appending `.pub`.
 */
class SSHKeyFile
{
    protected string $privateKeyFile;
    protected string $publicKeyFile;
    protected string $password = '';

    /**
     * @param string $privateKeyFile
     * @param string $password
     */
    public function __construct(string $privateKeyFile, string $password = '')
    {
        $this->privateKeyFile = realpath(str_replace('~', getenv('HOME'), $privateKeyFile));
        $this->publicKeyFile = sprintf('%s.pub', $this->privateKeyFile);
        if (!file_exists($this->privateKeyFile) || !file_exists($this->publicKeyFile)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Key file for login not found; either "%s" or "%s" was missing.',
                    $this->privateKeyFile,
                    $this->publicKeyFile
                )
            );
        }
        $this->password = $password;
    }

    public function getPrivateKeyFile(): string
    {
        return $this->privateKeyFile;
    }

    public function getPublicKeyFile(): string
    {
        return $this->publicKeyFile;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }
}