<?php

namespace Cli\Output;

class ThemeWithContentTypes extends Basic
{
    public static function echo(mixed $input = '', ContentType $type = ContentType::PARAGRAPH): void
    {
        /* Some content types may prepare the input and return a string */
        $handleInput = $type->handleInputCallback();
        if ($handleInput) {
            $input = $handleInput($input);
        }

        $lineStart = $type->startOfLine();
        $endOfLine = Color::RESET . $type->endOfLine();
        $width = $type->width();

        for ($i = 0; $i < $type->marginTop(); $i++) {
            echo $lineStart . "\n";
        }

        for ($i = 0; $i < $type->paddingTop(); $i++) {
            echo $type->color() . $type->background() . $lineStart;
            echo str_pad('', $width, ' ');
            echo $endOfLine;
        }

        echo $type->color() . $type->background() . $lineStart;
        echo str_pad($input, $width, ' ', STR_PAD_RIGHT);
        echo $endOfLine;

        for ($i = 0; $i < $type->paddingBottom(); $i++) {
            echo $type->color() . $type->background() . $lineStart;
            echo str_pad('', $width, ' ');
            echo $endOfLine;
        }

        for ($i = 0; $i < $type->marginBottom(); $i++) {
            echo $lineStart . "\n";
        }
    }
}