<?php /** @noinspection PhpMatchExpressionWithOnlyDefaultArmInspection */

namespace Cli\Output;

use Cli\Cli;
use InvalidArgumentException;

enum ContentType
{
    case TITLE;
    case HEADING;
    case PARAGRAPH;
    case COLUMNS;
    case COLUMN_LEFT;
    case COLUMN_RIGHT;
    case PROMPT;
    case WARNING;
    case ERROR;
    case ERROR_HEADING;
    case SUCCESS;
    case LIST_ITEM;
    case SELECT_ITEM;
    case MULTISELECT_ITEM;
    case LINE;

    public const BULLET_POINT = '🞄 ';
//    public const BULLET_POINT = '– ';
//    public const BULLET_POINT = '⬦ ';
//    public const BULLET_POINT = '🢒 ';

    public function color(): string
    {
        return match ($this) {
            self::TITLE, self::HEADING => Color::FG_WHITE,
            self::ERROR_HEADING => Color::FG_WHITE . Color::BOLD,
            self::COLUMN_RIGHT => Color::FG_LIGHT_BLUE . Color::BOLD,
            self::WARNING => Color::FG_ORANGE . Color::BOLD,
            self::ERROR => Color::FG_RED . Color::BOLD,
            self::SUCCESS => Color::FG_GREEN . Color::BOLD,
            default => '',
        };
    }

    public function background(): string
    {
        return match ($this) {
            self::TITLE, self::HEADING => Color::BG_BLUE,
            self::ERROR_HEADING => Color::BG_RED,
            default => '',
        };
    }

    public function startOfLine(): string|LineStartNumbers
    {
        $padding = '░  ';

        /* Example with line numbers (or: numbers of calls to line()) */
//        $padding = $this->lineNumbers();

        /* For debugging purposes: show file and line-no for each new output-line */
//        $padding = $this->debugOutput();

        return match ($this) {
            self::LINE => '',
            self::LIST_ITEM => $padding . self::BULLET_POINT,
            default => $padding,
        };
    }

    public function endOfLine(): string
    {
        return match ($this) {
            self::PROMPT => '',
            default => "\n",
        };
    }

    public function width(): int
    {
        return match ($this) {
            /* Example with line numbers: decrease width by string length of startOfLine */
//            self::TITLE, self::HEADING, self::ERROR_HEADING => max((int)floor(Cli::getWidth() / 2), 30) - 5,

            self::TITLE, self::HEADING, self::ERROR_HEADING => max((int)floor(Cli::getWidth() / 2), 30),
            self::COLUMN_LEFT => 26,
            default => 0,
        };
    }

    public function marginTop(): int
    {
        return match ($this) {
            self::TITLE, self::ERROR_HEADING => 1,
            self::HEADING => 2,
            default => 0,
        };
    }

    public function marginBottom(): int
    {
        return match ($this) {
            self::TITLE, self::HEADING => 1,
            default => 0,
        };
    }

    public function paddingTop(): int
    {
        return match ($this) {
            self::TITLE => 1,
            default => 0,
        };
    }

    public function paddingBottom(): int
    {
        return match ($this) {
            self::TITLE => 1,
            default => 0,
        };
    }

    /**
     * Definitions for special cases where the input requires further handling before being printed as a string.
     *
     * @return callable|false
     */
    public function handleInputCallback(): callable|false
    {
        return match ($this) {
            /* Column display; left column has a fixed width */
            self::COLUMNS => function (array $input): string {
                $left = array_shift($input);
                $right = array_shift($input);
                $leftWidth = max(mb_strlen($left), ContentType::COLUMN_LEFT->width());
                $text = Color::string(
                    str_pad($left, $leftWidth, ' ', STR_PAD_RIGHT),
                    ContentType::COLUMN_LEFT->color() . ContentType::COLUMN_LEFT->background()
                );
                $text .= Color::string(
                    $right,
                    ContentType::COLUMN_RIGHT->color() . ContentType::COLUMN_RIGHT->background()
                );
                return $text;
            },

            /* Multiselect items with checkbox */
            self::SELECT_ITEM => function (array $item): string {
                if (!isset($item['label'])) {
                    $item['label'] = $item['key'];
                }
                $line = Color::string(
                    str_pad($item['key'], $item['width'], ' ', STR_PAD_LEFT),
                    Color::FG_LIGHT_BLUE . Color::BOLD
                );
                $line .= '  ';
                $line .= Color::string($item['label'], Color::FG_LIGHT_BLUE);
                return $line;
            },

            /* Multiselect items with checkbox */
            self::MULTISELECT_ITEM => function (array $item): string {
                $selected = $item['selected'] ?? false;
                $line = Color::string(
                    str_pad($item['key'], $item['width'], ' ', STR_PAD_LEFT),
                    Color::FG_LIGHT_BLUE . Color::BOLD
                );
                $line .= '  ';
                $line .= sprintf('[%s] ', $selected ? 'X' : ' ');
                $line .= Color::string($item['label'], Color::FG_LIGHT_BLUE);
                return $line;
            },

            /* Horizontal rule */
            self::LINE => function (string $char): string {
                if(empty($char)) {
                    throw new InvalidArgumentException('ContentType::LINE requires some character to draw the line with.');
                }
                $line = '';
                for($len = Cli::getWidth() / mb_strlen($char), $i = 0; $i < $len; $i++) {
                    $line .= $char;
                }
                return $line;
            },

            default => false,
        };
    }

    private function debugOutput(): string
    {
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 4);
        return str_pad(
                pathinfo($backtrace[3]['file'], PATHINFO_FILENAME) . ':' . $backtrace[3]['line'],
                20,
                ' ',
                STR_PAD_RIGHT
            ) . '  ';
    }

    private function lineNumbers(): LineStartNumbers
    {
        return new LineStartNumbers();
    }
}
