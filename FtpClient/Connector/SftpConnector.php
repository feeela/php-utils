<?php

namespace FtpClient\Connector;

/**
 * This class provides FTP commands using a SSH user with either password or certificate-based login.
 *
 * Certificates may be ssh-rsa & ssh-dss only; see: https://www.php.net/manual/en/function.ssh2-connect.php
 *
 * All commands in this class expect absolute path names to files and directories.
 */
class SftpConnector implements ConnectorInterface
{
    /**
     * @var false|resource The SSH connection
     */
    protected $connection;
    /**
     * @var false|resource The SFTP subsystem for the SSH connection
     */
    protected $sftp;

    /**
     * @param string                                      $host
     * @param string                                      $user
     * @param string|\FtpClient\Connector\SSHKeyFile|null $auth Password or certificate-details
     *
     * @throws LoginFailedException
     */
    public function __construct(string $host, string $user, string|SSHKeyFile $auth = null)
    {
        $this->connection = ssh2_connect($host, 22);

        $useCertificate = $auth instanceof SSHKeyFile;
        if ($useCertificate) {
            /* Use certificate authentication */
            $authStatus = ssh2_auth_pubkey_file(
                $this->connection,
                $user,
                $auth->getPublicKeyFile(),
                $auth->getPrivateKeyFile(),
                $auth->getPassword()
            );
        } elseif (!empty($auth)) {
            /* Use password authentication */
            $authStatus = ssh2_auth_password($this->connection, $user, $auth);
        } else {
            /* no login */
            $authStatus = ssh2_auth_none($this->connection, $user);
        }

        if (!$authStatus) {
            throw new LoginFailedException(
                sprintf(
                    'Unable to login for "%s@%s" using %s.',
                    $user,
                    $host,
                    $useCertificate ? 'certificate ' . $auth : 'password'
                )
            );
        }

        $this->sftp = ssh2_sftp($this->connection);
    }

    public function __destruct()
    {
        ssh2_disconnect($this->connection);
    }

    protected function getSSHFilename(string $filename): string
    {
        return 'ssh2.sftp://' . intval($this->sftp) . '/' . ltrim($filename, '/');
    }

    public function get(string $remote, string $local): bool
    {
        if (!$this->isFile($remote)) {
            return false;
        }
        $localDir = dirname($local);
        if (!is_dir($localDir)) {
            shell_exec('mkdir -p ' . $localDir);
        }
        return false !== @file_put_contents($local, @file_get_contents($this->getSSHFilename($remote)));
    }

    /**
     * @param string $local  Absolute file path
     * @param string $remote Absolute file path
     *
     * @return bool
     */
    public function put(string $local, string $remote): bool
    {
        $dirname = dirname($remote);
        if (!$this->isDir($dirname)) {
            $this->mkdir($dirname, true);
        }
        $data = @file_get_contents($local);
        if ($data === false) {
            return false;
        }
        return @file_put_contents($this->getSSHFilename($remote), $data);
    }

    /**
     * Aborts, if $to already exists
     *
     * @param string $from Absolute remote file path
     * @param string $to   Absolute remote file path
     *
     * @return bool
     */
    public function rename(string $from, string $to): bool
    {
        if ($this->isFile($to)) {
            return false;
        }

        /* If the file was renamed to be in another directory, create that directory first, if necessary */
        $targetDirectory = dirname($to);
        if (!$this->isDir($targetDirectory)) {
            $this->mkdir($targetDirectory, true);
        }

        return rename($this->getSSHFilename($from), $this->getSSHFilename($to));
    }

    /**
     * Delete a file or directory
     *
     * @param string $path
     * @param bool   $recursive
     *
     * @return bool|array Returns an array on error when $recursive = true, listing all not-deleted files.
     */
    public function delete(string $path, bool $recursive = false): bool|array
    {
        if ($this->isDir($path)) {
            $notDeleted = [];
            if ($recursive) {
                foreach (array_reverse($this->list($path, true, true)) as $childPath => $childType) {
                    $toDeleteChild = $this->getSSHFilename($path . '/' . $childPath);
                    if ($childType == 'd') {
                        if (!rmdir($toDeleteChild)) {
                            $notDeleted[] = $toDeleteChild;
                        }
                    } elseif (!unlink($toDeleteChild)) {
                        $notDeleted[] = $toDeleteChild;
                    }
                }
                if (!rmdir($this->getSSHFilename($path))) {
                    $notDeleted[] = $path;
                }
                return !empty($notDeleted) ? $notDeleted : true;
            } else {
                return rmdir($this->getSSHFilename($path));
            }
        } else {
            return unlink($this->getSSHFilename($path));
        }
    }

    /**
     * Returns a flat array with file- and directory names as keys and the respective type ("f" | "d") as value.
     *
     * @param string $directory  Absolute remote path
     * @param bool   $recursive  Include all sub-folders and their contents.
     * @param bool   $showHidden Also list dot-files. Current and parent directory ("." & "..") are always hidden.
     *
     * @return array|false
     */
    public function list(string $directory, bool $recursive = false, bool $showHidden = false): array|false
    {
        if (!$this->isDir($directory)) {
            return false;
        }

        $sshDir = $this->getSSHFilename($directory);
        $dh = opendir($sshDir);
        $dirContent = [];
        while (($file = readdir($dh)) !== false) {
            /* Skip directories */
            if ($file === '.' || $file === '..') {
                continue;
            }
            /* Skip hidden files */
            if (!$showHidden && str_starts_with($file, '.')) {
                continue;
            }
            $currentFileIsDir = $this->isDir($directory . '/' . $file);
            $dirContent[$file] = $currentFileIsDir ? 'd' : 'f';
            if ($recursive && $currentFileIsDir) {
                $tmp = $this->list($directory . '/' . $file, $recursive, $showHidden);
                foreach ($tmp as $childName => $childType) {
                    $dirContent[$file . '/' . $childName] = $childType;
                }
            }
        }
        closedir($dh);
        return $dirContent;
    }

    /**
     * Test if a file or directory exists.
     *
     * @param string $filename
     *
     * @return bool
     */
    public function isFile(string $filename): bool
    {
        return file_exists($this->getSSHFilename($filename));
    }

    /**
     * Test if the given path is a directory.
     *
     * @param string $directory
     *
     * @return bool
     */
    public function isDir(string $directory): bool
    {
        return is_dir($this->getSSHFilename($directory));
    }

    public function mkdir(string $directory, bool $recursive = false, int $permissions = 0775): bool
    {
        return mkdir($this->getSSHFilename($directory), $permissions, $recursive);
    }

    public function chmod(string $path, int $permissions): bool
    {
        return ssh2_sftp_chmod($this->sftp, $path, $permissions);
    }
}