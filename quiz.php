#!/usr/bin/env php
<?php
/**
 * Demo file for the CLI tools.
 *
 * Demonstrates:
 * - styled output
 * - user prompt
 * - select with shorter keys
 * - select multiple items (multiselect)
 * - error handler with themed output
 */

use Cli\Cli;
use Cli\Output\ContentType;
use Cli\Output\ThemeWithContentTypes;

require_once('Autoloader.php');
new Autoloader([
    'baseDir' => __DIR__,
    'includeDirs' => '.',
]);

Cli::enforceCli();
Cli::setOutput(new ThemeWithContentTypes());
system('clear');

$questions = [
    [
        'question' => 'What is 5×5?',
        'accept' => 25,
    ],

    [
        'question' => 'Which fruit is yellow?',
        'answers' => [
            'Tomato',
            'Peas',
            'Banana',
            'Strawberry',
        ],
        'accept' => 'Banana',
    ],

    [
        'question' => 'Select yellow fruits',
        'answers' => [
            'Pineapple',
            'Peas',
            'Banana',
            'Strawberry',
        ],
        'accept' => ['Banana', 'Pineapple'],
    ],
];

Cli::echo('Quiz', ContentType::TITLE);


foreach ($questions as $question) {
    Cli::echo('', ContentType::HEADING);

    if (empty($question['answers'])) {
        /* Question with free input for the answer */
        if ($question['accept'] == Cli::prompt($question['question'])) {
            Cli::echo('Correct', ContentType::SUCCESS);
        } else {
            Cli::echo('Wrong', ContentType::ERROR);
        }
    } else {
        /* Variant with select */
        $options = [];
        foreach ($question['answers'] as $key => $answer) {
            $options[1 + $key] = [
                'label' => $answer,
            ];
        }

        if (is_array($question['accept'])) {
            /* Multiple answers */
            $correctAnswerCount = 0;
            foreach (
                Cli::multiselect(
                    $options,
                    'ok',
                    'ok',
                    $question['question']
                ) as $key => $answer
            ) {
                if (in_array($answer['label'], $question['accept'])) {
                    $correctAnswerCount++;
                } else {
                    $correctAnswerCount--;
                }
            }

            if ($correctAnswerCount == count($question['accept'])) {
                Cli::echo('Correct', ContentType::SUCCESS);
            } else {
                Cli::echo('Wrong', ContentType::ERROR);
            }
        } else {
            /* Single answer */
            $answer = Cli::select(
                $options,
                null,
                $question['question']
            );
            if (isset($options[$answer]) && $options[$answer]['label'] == $question['accept']) {
                Cli::echo('Correct', ContentType::SUCCESS);
            } else {
                Cli::echo('Wrong', ContentType::ERROR);
            }
        }
    }
}
