<?php

namespace FtpClient;

use FtpClient\Connector\ConnectorInterface;

/**
 * A simple FTP client for the 80% use-case.
 *
 * **Features:**
 * - Supports plain FTP connections and SFTP via SSH login.
 * - Provides recursive directory deletion.
 * - Missing local and remote paths to a file are created automatically if not passed;
 *   that means the relative file path will be the same locally and remote.
 * - Each command is immediately executed.
 * - Work with relative paths by setting the $localWorkingDirectory and $remoteWorkingDirectory.
 * - No output, no logging – just the minimal FTP client experience.
 *
 * To authenticate to a FTP host, you'll have to provide the credentials as a configuration-array
 * to the client constructor or pass in an instance of a ConnectorInterface implementation.
 *
 * **Example:**
 * ```php
 * $client = new Client([
 *              'host' => 'example.org',
 *              'user' => 'anonymous',
 *           ], getcwd(), '/remote/base');
 *
 * $client->put('some/path/file1');
 * $client->put('some/path/file2', 'my/path/file2');
 *
 * // creates on remote:
 * //   /remote/base/some/path/file1
 * //   /remote/base/my/path/file2
 *
 * $client->get('my/path/file2');
 *
 * // creates on local:
 * //   ./my/path/file2
 * ```
 *
 * **Authentication methods:**
 * ```php
 * // Anonymous FTP
 * $client = new Client([
 *              'host' => 'example.org',
 *              'user' => 'anonymous',
 *           ]);
 *
 * // plain FTP using username & password
 * $client = new Client([
 *              'host' => 'example.org',
 *              'user' => 'ftp-user',
 *              'password' => 'p4$$w0rd',
 *           ]);
 *
 * // SSH login using password
 * $client = new Client([
 *              'host' => 'example.org',
 *              'user' => 'ssh-user',
 *              'password' => 'p4$$w0rd',
 *              'useSSH' => true,
 *           ]);
 *
 * // SSH login using key file
 * // "keyFile" is the path
 * $client = new Client([
 *              'host' => 'example.org',
 *              'user' => 'ftp-user',
 *              'useSSH' => true,
 *              'keyFile' => '~/.ssh/id_rsa',
 *           ]);
 *
 * // Providing a Connector instance
 * $connector = new SftpConnector('example.org', 'ftp-user', new SSHCertificate('~/.ssh/id_rsa'));
 * $client = new Client($connector);
 * ```
 *
 * @see \FtpClient\Config::__construct() **The configuration-array is digested in the Config class and all options are explained there.**
 * @see \FtpClient\Connector\SftpConnector See the ConnectorInterface implementations for more details and quirks.
 * @see \FtpClient\Connector\FtpConnector See the ConnectorInterface implementations for more details and quirks.
 */
class Client implements CommandInterface
{
    protected ConnectorInterface $connector;
    protected string $localWorkingDirectory;
    protected string $remoteWorkingDirectory;

    /**
     * @param array|\FtpClient\Connector\ConnectorInterface $connector Config options as described at \FtpClient\Config::__construct() or Connector instance
     * @param string                                        $localWorkingDirectory
     * @param string                                        $remoteWorkingDirectory
     */
    public function __construct(
        array|ConnectorInterface $connector,
        string $localWorkingDirectory = '.',
        string $remoteWorkingDirectory = '/',
    ) {
        if ($connector instanceof ConnectorInterface) {
            $this->connector = $connector;
        } else {
            $auth = new Config($connector);
            $this->connector = $auth->getConnector();
        }
        $this->setLocalWorkingDirectory($localWorkingDirectory);
        $this->setRemoteWorkingDirectory($remoteWorkingDirectory);
    }

    public function setLocalWorkingDirectory(string $localWorkingDirectory): void
    {
        $this->localWorkingDirectory = rtrim(
                str_replace('~', getenv('HOME'), $localWorkingDirectory),
                '/'
            ) . '/';
    }

    public function getLocalWorkingDirectory(): string
    {
        return $this->localWorkingDirectory;
    }

    public function setRemoteWorkingDirectory(string $remoteWorkingDirectory): void
    {
        $this->remoteWorkingDirectory = rtrim($remoteWorkingDirectory, '/') . '/';
    }

    public function getRemoteWorkingDirectory(): string
    {
        return $this->remoteWorkingDirectory;
    }

    public function getConnector(): ConnectorInterface
    {
        return $this->connector;
    }

    public function get(string $remote, string $local = null): bool
    {
        if (is_null($local)) {
            $local = $remote;
        }
        return $this->connector->get(
            $this->remoteWorkingDirectory . $remote,
            $this->localWorkingDirectory . $local
        );
    }

    public function put(string $local, string $remote = null): bool
    {
        if (is_null($remote)) {
            $remote = $local;
        }
        if (is_dir($this->localWorkingDirectory . $local)) {
            return false;
        }
        return $this->connector->put(
            $this->localWorkingDirectory . $local,
            $this->remoteWorkingDirectory . $remote
        );
    }

    public function rename(string $from, string $to): bool
    {
        return $this->connector->rename($this->remoteWorkingDirectory . $from, $this->remoteWorkingDirectory . $to);
    }

    public function delete(string $path, bool $recursive = false): bool|array
    {
        return $this->connector->delete($this->remoteWorkingDirectory . $path, $recursive);
    }

    public function list(string $directory, bool $recursive = false, bool $showHidden = false): array|false
    {
        return $this->connector->list($this->remoteWorkingDirectory . $directory, $recursive, $showHidden);
    }

    public function isFile(string $filename): bool
    {
        return $this->connector->isFile($this->remoteWorkingDirectory . $filename);
    }

    public function isDir(string $directory): bool
    {
        return $this->connector->isDir($this->remoteWorkingDirectory . $directory);
    }

    public function mkdir(string $directory, bool $recursive = false, int $permissions = 0775): bool
    {
        return $this->connector->mkdir($this->remoteWorkingDirectory . $directory, $recursive, $permissions);
    }

    public function chmod(string $path, int $permissions): bool
    {
        return $this->connector->chmod($this->remoteWorkingDirectory . $path, $permissions);
    }
}