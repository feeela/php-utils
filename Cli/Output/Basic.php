<?php

namespace Cli\Output;

class Basic
{
    public static function echo(string|array $input): void
    {
        echo (is_array($input) ? implode(', ', $input) : $input) . "\n";
    }
}
