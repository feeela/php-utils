<?php

namespace FtpClient;

use FtpClient\Connector\ConnectorInterface;
use FtpClient\Connector\FtpConnector;
use FtpClient\Connector\SftpConnector;
use FtpClient\Connector\SSHKeyFile;

class Config
{
    private ConnectorInterface $connector;

    /**
     * The possible configuration options are:
     * ```php
     * $config = [
     *   'host' => (string)
     *      Required hostname
     *
     *   'user' => (string)
     *      Required username
     *
     *   'password' => (string|callable|null)
     *      Optional password;
     *      If set to null and no "keyFile" was specified, password-less login is assumed;
     *      "password" is ignored when "keyFile" is set;
     *      Can be a callback that must return a string, which is used as password
     *
     *   'useSSH' => (bool)
     *      Optional flag to use a SSH connection instead of plain FTP; default: FALSE
     *      "keyFile" and "keyFilePassword" are ignored if "useSSH" is false
     *
     *   'keyFile' => (string|null)
     *      Optional path to private key file for certificate login;
     *      The public key file is guessed by appending ".pub"
     *
     *   'keyFilePassword' => (string|null)
     *      Optional password for the key file
     * ]
     * ```
     *
     * @param array $config
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $config)
    {
        if (empty($config['host']) || empty($config['user'])) {
            throw new \InvalidArgumentException(
                'To authenticate to a FTP connection you\'ll need to provide "host" & "user" at least.'
            );
        }

        $config['useSSH'] = $config['useSSH'] ?? false;
        if(is_callable($config['password'])) {
            $config['password'] = $config['password']();
        }

        if ($config['useSSH']) {
            /* SSH connection */
            if (!empty($config['keyFile'])) {
                /* Certificate authentication */
                $auth = new SSHKeyFile($config['keyFile'], $config['keyFilePassword'] ?? null);
            } else {
                $auth = $config['password'] ?? null;
            }
            $this->connector = new SftpConnector($config['host'], $config['user'], $auth);
        } else {
            /* plain FTP */
            $this->connector = new FtpConnector($config['host'], $config['user'], $config['password'] ?? '');
        }
    }

    public function getConnector(): ConnectorInterface
    {
        return $this->connector;
    }
}