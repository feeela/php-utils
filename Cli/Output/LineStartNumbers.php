<?php

namespace Cli\Output;

/**
 * Helper to print line numbers
 */
class LineStartNumbers implements \Stringable
{
    protected static int $lineNumber = 0;

    public function __toString(): string
    {
        self::$lineNumber++;
        return str_pad(self::$lineNumber, 3, ' ', STR_PAD_LEFT) . '  ';
    }
}
