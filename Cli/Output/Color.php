<?php

namespace Cli\Output;

class Color
{
    public const RESET = "\033[0m";
    public const BOLD = "\033[01m";
    public const DISABLE = "\033[02m";
    public const UNDERLINE = "\033[04m";
    public const REVERSE = "\033[07m";
    public const INVISIBLE = "\033[08m";

    public const FG_BLACK = "\033[30m";
    public const FG_RED = "\033[31m";
    public const FG_GREEN = "\033[32m";
    public const FG_ORANGE = "\033[33m";
    public const FG_BLUE = "\033[34m";
    public const FG_PURPLE = "\033[35m";
    public const FG_CYAN = "\033[36m";
    public const FG_LIGHT_GREY = "\033[37m";
    public const FG_DARK_GREY = "\033[90m";
    public const FG_LIGHT_RED = "\033[91m";
    public const FG_LIGHT_GREEN = "\033[92m";
    public const FG_YELLOW = "\033[93m";
    public const FG_LIGHT_BLUE = "\033[94m";
    public const FG_PINK = "\033[95m";
    public const FG_LIGHT_CYAN = "\033[96m";
    public const FG_WHITE = "\033[97m";

    public const BG_BLACK = "\033[40m";
    public const BG_RED = "\033[41m";
    public const BG_GREEN = "\033[42m";
    public const BG_ORANGE = "\033[43m";
    public const BG_BLUE = "\033[44m";
    public const BG_PURPLE = "\033[45m";
    public const BG_CYAN = "\033[46m";
    public const BG_LIGHT_GREY = "\033[47m";
    public const BG_DARK_GRAY = "\033[100m";
    public const BG_LIGHT_RED = "\033[101m";
    public const BG_LIGHT_GREEN = "\033[102m";
    public const BG_LIGHT_YELLOW = "\033[103m";
    public const BG_LIGHT_BLUE = "\033[104m";
    public const BG_LIGHT_MAGENTA = "\033[105m";
    public const BG_LIGHT_CYAN = "\033[106m";
    public const BG_WHITE = "\033[107m";

    /**
     * @param string     $text
     * @param string     $color
     * @param array|null $replacements
     *
     * @return string
     */
    public static function string(string $text, string $color = '', ?array $replacements = null): string
    {
        if (!is_null($replacements)) {
            $text = sprintf($text, ...$replacements);
        }
        return $color . $text . Color::RESET;
    }
}
