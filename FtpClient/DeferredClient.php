<?php

namespace FtpClient;

/**
 * FTP as batch commands that may be executed multiple times.
 *
 * This FTP client stores the commands `get`, `put`, `rename`, `delete`, `mkdir` & `chmod`
 * and **executes those only when execute() is called**.
 * Therefore, the return values of the those methods are always true; see the key and value
 * as returned from the execute() generator to get information about each commands status.
 *
 * This allows to up- or download several files to multiple local or remote locations
 * by setting the respective working dirs and calling execute() multiple times.
 *
 * **Example:**
 * ```php
 * $client = new DeferredClient([
 *              'host' => 'example.org',
 *              'user' => 'anonymous',
 *           ]);
 * $client->put('local/file1', 'remote/file1');
 * $client->put('local/file2', 'remote/file2');
 *
 * $remoteDirs = ['/A', '/B'];
 * foreach ($remoteDirs as $remoteBaseDir) {
 *   $client->setRemoteWorkingDirectory($remoteBaseDir);
 *   foreach ($client->execute() as $status => $details) {
 *     echo 'Upload ' . ($status ? 'successful' : 'failed') . "\n";
 *   }
 * }
 *
 * // creates on remote:
 * //   /A/remote/file1
 * //   /A/remote/file2
 * //   /B/remote/file1
 * //   /B/remote/file2
 * ```
 *
 * **Creating a DeferredClient from an existing Client/connection:**
 * ```php
 * $client = new Client([
 *              'host' => 'example.org',
 *              'user' => 'anonymous',
 *           ], getcwd(), '/remote/base');
 *
 * $deferredClient = new DeferredClient(
 *                      $client->getConnector(),
 *                      $client->getLocalWorkingDirectory(),
 *                      $client->getRemoteWorkingDirectory(),
 *                   );
 * ```
 */
class DeferredClient extends Client
{
    protected array $commands = [];

    /**
     * A generator method that executes one command of the batch-list per call.
     *
     * Usage in a loop:
     * ```php
     * foreach ($client->execute() as $status => $details) {
     *   echo 'Command ' . $details['command'] . ($status ? 'successful' : 'failed') . "\n";
     * }
     * ```
     *
     * Usage for a single call:
     * ```php
     * $res = $client->execute();
     * echo 'Command ' . $res->value()['command'] . ($res->key() ? 'successful' : 'failed') . "\n";
     * ```
     *
     * @return \Generator
     */
    public function execute(): \Generator
    {
        foreach ($this->commands as $args) {
            $command = array_shift($args);
            $ret = parent::$command(...$args);
            yield $ret => [
                'command' => $command,
                'arguments' => $args,
            ];
        }
    }

    public function count(): int {
        return count($this->commands);
    }

    public function clear(): void {
        $this->commands = [];
    }

    public function get(string $remote, string $local = null): bool
    {
        $this->commands[] = [__FUNCTION__, $remote, $local];
        return true;
    }

    public function put(string $local, string $remote = null): bool
    {
        $this->commands[] = [__FUNCTION__, $local, $remote];
        return true;
    }

    public function rename(string $from, string $to): bool
    {
        $this->commands[] = [__FUNCTION__, $from, $to];
        return true;
    }

    public function delete(string $path, bool $recursive = false): bool|array
    {
        $this->commands[] = [__FUNCTION__, $path, $recursive];
        return true;
    }

    public function mkdir(string $directory, bool $recursive = false, int $permissions = 0775): bool
    {
        $this->commands[] = [__FUNCTION__, $directory, $recursive, $permissions];
        return true;
    }

    public function chmod(string $path, int $permissions): bool
    {
        $this->commands[] = [__FUNCTION__, $path, $permissions];
        return true;
    }
}