<?php

/**
 * A configurable class autoloader.
 *
 * The default config-array is:
 *
 * <code>
 * $configArray = array(
 *
 * // Necessary if the router is not stored in the same directory as e.g. src/;
 * 'baseDir' => '.',
 *
 * // Comma-separated string or array
 * 'includeDirs' => 'src,lib',
 *
 * // String to be skipped/trimmed from class name.
 * // Maybe also the first namespace-part, depending on 'useNamespaces'
 * 'skip' => null,
 *
 * // Comma-separated string or array, gets parsed with sprintf()
 * 'filePattern' => '%s.php,%s.inc',
 *
 * // Use namespaces as directories, appended to the 'includeDirs'
 * 'useNamespaces' => true,
 *
 * // Explode class name with 'classNameToDirDelimiter' to get directories;
 * // This is set to TRUE when 'useNamespaces' is TRUE
 * 'classNameToDir' => false,
 *
 * // This is overwritten with a backslash when 'useNamespaces' is TRUE
 * 'classNameToDirDelimiter' => '_',
 *
 * // This could be a callback to map a classname to a specific filename;
 * // If a callback is set and if it returns something else then FALSE,
 * // the returned value will be used for require_once
 * 'resolveStaticClassName' => null,
 *
 * // Transform class names to lowercase
 * 'lowercase' => false,
 *
 * );
 * </code>
 *
 * To use this autoloader create an instance of it:
 * <code>
 * new Autoloader( $configArray );
 * </code>
 *
 * @author Thomas Heuer <projekte@thomas-heuer.eu>
 */
class Autoloader
{
    private array $classLoaderConfig;

    /**
     * @param array $configArray will get merged with default config array
     */
    public function __construct(array $configArray = [])
    {
        /* merge array into default configuration */
        $this->classLoaderConfig = array_merge([
            'baseDir' => '.',
            'includeDirs' => 'src,lib',
            'skip' => null,
            'filePattern' => '%s.php,%s.inc',
            'useNamespaces' => true,
            'classNameToDir' => false,
            'classNameToDirDelimiter' => '_',
            'resolveStaticClassName' => null,
            'lowercase' => false,
        ], $configArray);

        /* register autoload function */
        spl_autoload_register([$this, 'classLoader'], true, true);
    }

    /**
     * A configurable class autoloader.
     *
     * @param string $className may use namespaces for folders
     *
     * @return bool
     */
    private function classLoader(string $className): bool
    {
        if (is_callable($this->classLoaderConfig['resolveStaticClassName'])) {
            $staticClassname = $this->classLoaderConfig['resolveStaticClassName']($className);
            if (false !== $staticClassname) {
                require_once $staticClassname;
                return true;
            }
        }

        if ($this->classLoaderConfig['lowercase']) {
            $className = strtolower($className);
        }

        /* skip first part of class name, if specified */
        if (!is_null($this->classLoaderConfig['skip'])
            && str_starts_with($className, $this->classLoaderConfig['skip'])) {
            $className = substr($className, strlen($this->classLoaderConfig['skip']));
        }

        /* Guess path to class file */
        if ($this->classLoaderConfig['useNamespaces']) {
            $filePath = ltrim(str_replace('\\', '/', $className), '/');
        } elseif ($this->classLoaderConfig['classNameToDir']) {
            $filePath = implode(
                DIRECTORY_SEPARATOR,
                explode($this->classLoaderConfig['classNameToDirDelimiter'], $className)
            );
        } else {
            $filePath = $className;
        }

        /* iterate over specified include directories */
        $fp = $this->getArray($this->classLoaderConfig['filePattern']);
        foreach ($this->getArray($this->classLoaderConfig['includeDirs']) as $incDir) {
            /* iterate over specified file pattern */
            foreach ($fp as $filePattern) {
                $classFilePath = @realpath(
                    $this->classLoaderConfig['baseDir'] . DIRECTORY_SEPARATOR . $incDir
                    . DIRECTORY_SEPARATOR . sprintf($filePattern, $filePath)
                );
                if (file_exists($classFilePath)) {
                    require_once $classFilePath;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns an array. If the value is scalar,
     * it will get exploded by $delimiter.
     *
     * @param mixed  $value
     * @param string $delimiter (default: ',')
     *
     * @return array
     */
    private function getArray(mixed $value, string $delimiter = ','): array
    {
        // return arrays and objects as arrays
        if (!is_scalar($value)) {
            return ( array )$value;
        }
        return explode($delimiter, $value);
    }
}
