<?php

namespace Cli;

use Cli\Output\ContentType;

/**
 * CLI ErrorHandler
 *
 * - Suppress normal output on error or exception
 * - Translate PHP errors into exceptions
 * - Print formatted exceptions to the screen
 *
 * Usage:
 * <code>
 * new ErrorHandler( E_ALL );
 * </code>
 *
 * @author Thomas Heuer <projekte@thomas-heuer.eu>
 */
class ErrorHandler
{

    /**
     * @param int|null $errorReporting Some PHP error reporting constant; the ini setting 'error_reporting' is used as default.
     */
    public function __construct(int $errorReporting = null)
    {
        /* setup error and exception handling */
        error_reporting(( int )($errorReporting ?? ini_get('error_reporting')));
        set_error_handler(array($this, 'errorHandler'));
        set_exception_handler(array($this, 'exceptionHandler'));
    }

    /**
     * @throws \ErrorException
     */
    public function errorHandler($errno, $errstr, $errfile, $errline)
    {
        if (!(error_reporting() & $errno)) {
            /* This error code is not included in error_reporting; do nothing */
            return null;
        }

        /* Throw error as exception */
        throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
    }

    public function exceptionHandler(\Throwable $exception): never
    {
        // get some meaningful string from error code
        $errorCode = array(
            1 => 'E_ERROR',
            2 => 'E_WARNING',
            4 => 'E_PARSE',
            8 => 'E_NOTICE',
            16 => 'E_CORE_ERROR',
            32 => 'E_CORE_WARNING',
            64 => 'E_COMPILE_ERROR',
            128 => 'E_COMPILE_WARNING',
            256 => 'E_USER_ERROR',
            512 => 'E_USER_WARNING',
            1024 => 'E_USER_NOTICE',
            2048 => 'E_STRICT',
            4096 => 'E_RECOVERABLE_ERROR',
            8192 => 'E_DEPRECATED',
            16384 => 'E_USER_DEPRECATED',
            30719 => 'E_ALL'
        );

        $errorCodeIdentifier = $errorCode[$exception->getCode()] ?? get_class($exception);

        $outputFilename = str_replace($_SERVER['DOCUMENT_ROOT'], '', str_replace('\\', '/', $exception->getFile()));

        Cli::echo('An error has occurred. Cannot proceed.', ContentType::ERROR_HEADING);
        Cli::echo('', ContentType::ERROR);
        Cli::echo(wordwrap($exception->getMessage(), 150), ContentType::ERROR);
        Cli::echo('', ContentType::ERROR);
        Cli::echo(sprintf("%s in %s@%d", $errorCodeIdentifier, $outputFilename, $exception->getLine()),
            ContentType::ERROR);

        $trace = $exception->getTrace();
        if(isset($trace[0]) && $trace[0]['function'] == 'errorHandler') {
            array_shift($trace);
        }
        if (!empty($trace)) {
            Cli::echo('', ContentType::ERROR);
            foreach ($trace as $i => $line) {
                $out = '#' . $i . ' ';
                if (empty($line['file'])) {
                    $out .= '[internal function]: ';
                } else {
                    $out .= $line['file'] . '(' . $line['line'] . '): ';
                }
                if (!empty($line['class'])) {
                    $out .= $line['class'] . $line['type'];
                }
                $out .= $line['function'] . '()';
                Cli::echo($out, ContentType::ERROR);
            }
        }
        Cli::echo('', ContentType::ERROR);
        exit;
    }

}
