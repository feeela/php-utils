# php-utils

A collection of my PHP code snippets.

A contents are published using the [MIT license](LICENSE).

## Classes

- [Autoloader](Autoloader.php) – A configurable class autoloader.
- [ErrorHandler](ErrorHandler.php) – Translate PHP errors into exceptions
- [IterableCountable](IterableCountable.php) – A countable iterator with additional magic functions.
- [KeyValueTable](KeyValueTable.php) – This is a simple key-value storage that creates it own database table to save the data to a MySQL database.
- [HashTable](HashTable.php) – An iterator, using a SHA1 hash as key. This iterator is able to use non-scalar values as key.
- [Lexicon Singleton class](Lexicon/Lexicon.php) for handling strings in different languages via a lexicon-database

## Functions

- [getArrayValueReference()](function.getArrayValueReference.php) – Return a reference to an array value specified by its key path.
- [getHtmlList()](function.getHtmlList.php) – This function generates a recurive HTML list from an array or an iterable object. By default, it returns an unordered list (UL). But you can change the templates through the $options argument.
- [enumToArray()](function.enumToArray.php) – Retrieve ENUM-values from a MySQL-column
- [getAlias()](function.getAlias.php) – Format a document title to be URL-safe.
- [array_kshift()](function.array_kshift.php) – Shift an element off the beginning of &$arr return that as single-entry array, using the original key for the value.
- [CSV helper](function.csvHelper.php)
    * `arrayToCsv()` – Return two-dimensional array as CSV string.
    * `readCsv()` – Read a CSV file, given an open resource handle.
    * `writeCsv()` – Write data to a CSV file.
- [Array sorting](function.sortArray.php)
    * `sortArray()` – Sort 2D-associative array by one $field. (ASC)
    * `sortArrayMultiple()` – Sort 2D-associative array by one or many $fields. (ASC and DESC) Extension of getArray(), to allow sorting to multiple columns.
- [mergeArrayRecursive()](function.mergeArrayRecursive.php) – Merges multiple associative arrays into one.

