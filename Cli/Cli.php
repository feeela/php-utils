<?php

namespace Cli;

use Cli\Output\Basic;
use Cli\Output\Color;
use Cli\Output\ContentType;

class Cli
{
    /**
     * Abort if this script is not executed from the command line.
     *
     * @return void
     */
    public static function enforceCli(): void
    {
        if (php_sapi_name() !== 'cli') {
            exit(1);
        }
    }

    protected static Basic $output;

    public static function setOutput(Basic $output): void
    {
        self::$output = $output;
    }

    public static function echo(string|array $text = '', ...$arguments): void
    {
        self::$output::echo($text, ...$arguments);
    }

    /**
     * Write a patch file for $file into $patchPath
     *
     * @param string $patchPath
     * @param string $file
     * @param string $diff
     *
     * @return string
     */
    public static function writePatchFile(string $patchPath, string $file, string $diff): string
    {
        /* Create a sub-directory for this patch */
        $deployPatchDir = $patchPath . dirname($file);
        if (!is_dir($deployPatchDir)) {
            mkdir($deployPatchDir, 0777, true);
        }

        $patchFile = $patchPath . $file . '.patch';
        file_put_contents($patchFile, $diff);
        return $patchFile;
    }

    /**
     * CLI user input handling.
     *
     * @param string        $message
     * @param string|null   $default
     * @param string[]|null $options
     * @param bool          $allowAny
     *
     * @return string|null
     */
    public static function prompt(
        string $message,
        string $default = null,
        array $options = null,
        bool $allowAny = false
    ): ?string {
        $optionsString = '';
        $optionsStringLength = 0;
        if (is_array($options)) {
            $outputOptions = [];
            foreach ($options as $option) {
                $optionsStringLength += mb_strlen($option) + 1;
                if ($option == $default) {
                    $outputOptions[] = Color::FG_ORANGE . $option . Color::FG_LIGHT_GREY;
                } else {
                    $outputOptions[] = $option;
                }
            }
            $optionsString = implode('|', $outputOptions);
        } elseif (!is_null($default)) {
            $optionsString = Color::FG_ORANGE . $default . Color::FG_LIGHT_GREY;
        }

        $label = Color::BOLD . $message . Color::RESET;
        if (!empty($optionsString)) {
            $label .= sprintf(" [%s]", Color::FG_LIGHT_GREY . $optionsString . Color::RESET);
        }
        $label .= ': ';
        Cli::echo($label, ContentType::PROMPT);

        $value = trim(fgets(STDIN));

        if (mb_strlen($value) > 0) {
            if ($allowAny === true
                || is_null($options)
                || in_array($value, $options)) {
                return $value;
            }
        }
        return $default;
    }

    /**
     * Asks for a user input until $getValueCallback returned a value, i.e. didn't throw an exception.
     *
     * @param callable    $getValueCallback
     * @param string      $message
     * @param null        $default
     * @param array|null  $options
     * @param bool        $allowAny
     * @param string|null $failureMessage
     *
     * @return array [string $userInput, mixed $callbackValue]
     */
    public static function promptUntilValue(
        callable $getValueCallback,
        string $message,
        ?string $failureMessage = null,
        $default = null,
        array $options = null,
        bool $allowAny = false
    ): array {
        do {
            $userInput = Cli::prompt(
                $message,
                $default,
                $options,
                $allowAny
            );

            try {
                $value = $getValueCallback($userInput);
            } catch (\Throwable $e) {
                $exceptionMessage = str_replace('ftp_login(): ', '', $e->getMessage());
                Cli::echo(
                    sprintf($failureMessage ?? 'That didn\'t work, try again. (error: %s)', $exceptionMessage),
                    ContentType::ERROR
                );
            }
        } while (!isset($value));

        return [$userInput, $value];
    }

    /**
     * Clean quit a CLI program and reset screen
     */
    public static function shutdown(): never
    {
        echo "\033c";                                        // Clear terminal
        system("tput cnorm && tput cup 0 0 && stty echo");   // Restore cursor default
        echo PHP_EOL;                                        // New line
        exit;                                                // Clean quit
    }

    protected static function listSelectItems($options): void
    {
        Cli::echo();
        $maxKeyLength = max(array_map('mb_strlen', array_keys($options)));
        foreach ($options as $key => $option) {
            $option['key'] = $key;
            $option['width'] = $maxKeyLength;
            Cli::echo($option, ContentType::SELECT_ITEM);
        }
        Cli::echo();
    }

    public static function menu(
        array $options,
        ?string $default = null,
        string $back = 'back',
        $message = 'Please choose an option',
        $allowAny = true
    ): void {
        self::listSelectItems($options);
        if (!$allowAny) {
            $promptOptions = [$back, ...array_keys($options)];
        } else {
            $promptOptions = [$back];
            if (!is_null($default) && $default != $back) {
                $promptOptions[] = $default;
            }
        }
        while ($back != $selected = self::prompt(
                $message,
                $default,
                $promptOptions,
                $allowAny
            )) {
            Cli::echo();

            if (!isset($options[$selected])) {
                Cli::echo('Not an option', ContentType::ERROR);
                self::listSelectItems($options);
                continue;
            }

            $options[$selected]['callback']($options[$selected]);
            self::listSelectItems($options);
        }
    }

    public static function select(
        array $options,
        ?string $default = null,
        $label = 'Please choose an option'
    ): string|int|null {
        self::listSelectItems($options);
        $promptOptions = [];
        if (!is_null($default)) {
            $promptOptions[] = $default;
        }
        return self::prompt(
            $label,
            $default,
            $promptOptions,
            true
        );
    }

    public static function multiselect(
        array $options,
        ?string $default = 'ok',
        string $back = 'ok',
        $label = 'Please choose an option'
    ): array {
        $countLineBreaks = 3;
        $lines = count($options) + $countLineBreaks;
        self::listMultiSelectItems($options);
        $promptOptions = [$back];
        if (!is_null($default) && $default != $back) {
            $promptOptions[] = $default;
        }
        while ($back != $selected = self::prompt(
                $label,
                $default,
                $promptOptions,
                true
            )) {
            echo chr(27) . '[0G';
            echo chr(27) . '[' . $lines . 'A';
            for ($l = 0; $l < $lines; $l++) {
                echo str_pad('', self::getWidth(), ' ', STR_PAD_RIGHT);
            }
            echo chr(27) . '[0G';
            echo chr(27) . '[' . $lines . 'A';

            Cli::echo();
            if (!isset($options[$selected])) {
                self::listMultiSelectItems($options);
                Cli::echo('Not an option', ContentType::ERROR);
                $lines = count($options) + $countLineBreaks + 1;
                continue;
            }
            $options[$selected]['selected'] = !($options[$selected]['selected'] ?? false);
            self::listMultiSelectItems($options);
            $lines = count($options) + $countLineBreaks;
        }

        return array_filter($options, function ($option) {
            return $option['selected'] ?? false;
        });
    }

    protected static function listMultiSelectItems(array $options): void
    {
        Cli::echo();
        $maxKeyLength = max(array_map('mb_strlen', array_keys($options)));
        foreach ($options as $key => $option) {
            $option['key'] = $key;
            $option['width'] = $maxKeyLength;
            Cli::echo($option, ContentType::MULTISELECT_ITEM);
        }
        Cli::echo();
    }

    public static function getWidth(): int
    {
        return intval(shell_exec('tput cols'));
    }
}

Cli::setOutput(new Basic());
