<?php

namespace FtpClient\Connector;

use FTP\Connection;

/**
 * This class provides FTP commands using a plainFTP connection with or without password.
 *
 * All commands in this class expect absolute path names to files and directories.
 */
class FtpConnector implements ConnectorInterface
{
    /**
     * @var \FTP\Connection|false The FTP connection
     */
    protected Connection|false $connection;

    /**
     * @param string $host
     * @param string $user
     * @param string $password
     *
     * @throws LoginFailedException
     */
    public function __construct(string $host, string $user = 'anonymous', string $password = '')
    {
        $this->connection = ftp_connect($host);
        if (!ftp_login($this->connection, $user, $password)) {
            throw new LoginFailedException(
                sprintf(
                    'Unable to login for "%s@%s".',
                    $user,
                    $host
                )
            );
        }
        ftp_pasv($this->connection, true);
    }

    public function __destruct()
    {
        ftp_close($this->connection);
    }

    public function get(string $remote, string $local): bool
    {
        if (!$this->isFile($remote)) {
            return false;
        }
        $localDir = dirname($local);
        if(!is_dir($localDir)) {
            shell_exec('mkdir -p ' . $localDir);
        }
        return ftp_get($this->connection, $local, $remote);
    }

    public function put(string $local, string $remote): bool
    {
        $this->mkdir(dirname($remote), true);
        return ftp_put($this->connection, $remote, $local);
    }

    public function rename(string $from, string $to): bool
    {
        $this->mkdir(dirname($to), true);
        return ftp_rename($this->connection, $from, $to);
    }

    public function delete(string $path, bool $recursive = false): bool|array
    {
        if ($this->isDir($path)) {
            if ($recursive) {
                $notDeleted = [];
                foreach (array_reverse($this->list($path, true, true)) as $childName => $childType) {
                    $toDeleteChild = $path . '/' . $childName;
                    if ($childType == 'd') {
                        if (!ftp_rmdir($this->connection, $toDeleteChild)) {
                            $notDeleted[] = $toDeleteChild;
                        }
                    } elseif (!ftp_delete($this->connection, $toDeleteChild)) {
                        $notDeleted[] = $toDeleteChild;
                    }
                }
                if (!ftp_rmdir($this->connection, $path)) {
                    $notDeleted[] = $path;
                }
                return !empty($notDeleted) ? $notDeleted : true;
            } else {
                return ftp_rmdir($this->connection, $path);
            }
        } else {
            return ftp_delete($this->connection, $path);
        }
    }

    public function list(string $directory, bool $recursive = false, bool $showHidden = false): array|false
    {
        if (!$this->isDir($directory)) {
            return false;
        }
        $files = ftp_mlsd($this->connection, $directory);
        $dirContent = [];
        foreach ($files as $file) {
            /* Skip current and parent directories */
            if ($file['name'] === '.' || $file['name'] === '..') {
                continue;
            }
            /* Skip hidden files */
            if (!$showHidden && str_starts_with($file['name'], '.')) {
                continue;
            }
            $currentFileIsDir = in_array($file['type'], ['dir', 'OS.unix=symlink']);
            $dirContent[$file['name']] = $currentFileIsDir ? 'd' : 'f';
            if ($recursive && $currentFileIsDir) {
                $tmp = $this->list($directory . '/' . $file['name'], $recursive, $showHidden);
                foreach ($tmp as $childName => $childType) {
                    $dirContent[$file['name'] . '/' . $childName] = $childType;
                }
            }
        }
        return $dirContent;
    }

    /**
     * Test if a file or directory exists.
     *
     * @param string $filename
     *
     * @return bool
     */
    public function isFile(string $filename): bool
    {
        return !(ftp_size($this->connection, $filename) == -1);
    }

    /**
     * Test if the given path is a directory.
     *
     * @param string $directory
     *
     * @return bool
     */
    public function isDir(string $directory): bool
    {
        $pwd = ftp_pwd($this->connection);

        if (@ftp_chdir($this->connection, $directory)) {
            ftp_chdir($this->connection, $pwd);
            return true;
        } else {
            ftp_chdir($this->connection, $pwd);
            return false;
        }
    }

    public function mkdir(string $directory, bool $recursive = false, int $permissions = null): bool
    {
        if (!is_null($permissions)) {
            trigger_error(
                'Setting of directory $permissions is not implemented for \FtpClient\Connector\FtpConnector::mkdir()',
                E_USER_WARNING
            );
        }

        if ($this->isDir($directory)) {
            return false;
        }

        if (!$recursive) {
            return false !== ftp_mkdir($this->connection, $directory);
        }

        $result = false;
        $pwd = ftp_pwd($this->connection);
        $parts = explode('/', $directory);

        foreach ($parts as $part) {
            if ($part == '') {
                continue;
            }

            if (!@ftp_chdir($this->connection, $part)) {
                $result = ftp_mkdir($this->connection, $part);
                ftp_chdir($this->connection, $part);
            }
        }

        ftp_chdir($this->connection, $pwd);

        return $result !== false;
    }

    public function chmod(string $path, int $permissions): bool
    {
        return (bool)ftp_chmod($this->connection, $permissions, $path);
    }
}